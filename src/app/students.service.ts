import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Student } from './interfaces/student';

@Injectable({
  providedIn: 'root'
})

export class StudentsService {

  userCollection:AngularFirestoreCollection;
  studentsCollection:AngularFirestoreCollection =this.db.collection('students');;

  getStudents(): Observable<any[]> {
    this.studentsCollection = this.db.collection(`students`, 
       ref => ref.limit(10).orderBy('index','desc'))
    return this.studentsCollection.snapshotChanges();    
  } 
  deleteStudents(id:string){
    this.db.doc(`students/${id}`).delete();
  }
  addStudent(index:number,userEmail:string, math:number, psycho:number, pay:boolean, result:string){
    const student:Student = {index:index,userEmail:userEmail,math:math,psycho:psycho, pay:pay, result:result}
    this.studentsCollection.add(student)
  } 
  
  

  constructor(private db: AngularFirestore) { }
}
