import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  userId;
  students:Student[];
  students$;
  displayedColumns: string[] = ['email','math', 'psycho', 'pay','Delete', 'Result'];
  deleteStudent(index){
    let id = this.students[index].id;
    this.studentService.deleteStudents(id);
  }
  constructor(   private authService:AuthService, private studentService: StudentsService) { }

  ngOnInit(): void {
          this.students$ = this.studentService.getStudents();
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const students:Student = document.payload.doc.data();             
                students.id = document.payload.doc.id;
                   this.students.push(students); 
              }                        
            }
          );
      }
  }   
