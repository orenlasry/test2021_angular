import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = "https://qqppisfkjf.execute-api.us-east-1.amazonaws.com/beta";

  predict(math:number, psycho:number, pay:boolean){
    let json= {
      'data':{
        "math":math,
        "psycho":psycho,
        "pay":pay
      }
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res=>{
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','')
        final = final.replace(']','')
        return final
      })
    )
    
  }

  constructor(private http:HttpClient) { }
}