import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { PredictService } from '../predict.service';
import { StudentsService } from '../students.service';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  math:number;
  psycho:number;
  pay:boolean;
  Predictresult:string = null;
  index:number = 0;
  userEmail;
  predictions = [
    {value: 'Will dropout', viewValue: 'Will dropout'},
    {value: 'Will not dropout', viewValue: 'Will not dropout'},
    
  ];

onSubmit(){
  console.log(this.userEmail)
  this.index++;
  this.studentService.addStudent(this.index,this.userEmail, this.math, this.psycho, this.pay, this.Predictresult)
  this.router.navigate(['/students']);
}
predict(math, psycho, pay){
  this.predictService.predict(math, psycho, pay).subscribe(
      res =>{
        console.log(res);
        if (res>0.5){
          var result = "Will not dropout"}
          else{
            var result = "Will dropout"
          }
          this.Predictresult = result;
    }
   
  )

}
  constructor( private authService:AuthService, private predictService:PredictService, private studentService:StudentsService,private router:Router ) { }

  ngOnInit(): void {
      this.authService.getUser().subscribe(
        res=>{
          this.userEmail = res.email
        }
      )
    }
  }


