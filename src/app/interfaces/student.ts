export interface Student {
    id?:string,
    index:number
    userEmail?
    math:number,
    psycho:number,
    pay:boolean,
    result?:string
}
