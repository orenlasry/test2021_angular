import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userEmail:string;
  user:Observable<User | null>;

  login(email:string, password:string){
   return this.afAuth.signInWithEmailAndPassword(email, password)

  }
  register(email:string, password:string){
   return this.afAuth.createUserWithEmailAndPassword(email, password)
  }

  logout(){
    this.afAuth.signOut();
    this.router.navigate(['/login'])
  }
  getUser():Observable<User |null>{
    return this.user;
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;
  }
}
