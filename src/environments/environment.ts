// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCxUkosEhZBpqcg5pYMCWl1m2rmg8QfWzk",
    authDomain: "test2021-jce.firebaseapp.com",
    projectId: "test2021-jce",
    storageBucket: "test2021-jce.appspot.com",
    messagingSenderId: "412217759184",
    appId: "1:412217759184:web:743935dde76e0cf56195d5",
    measurementId: "G-ME3H67ZCD0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
